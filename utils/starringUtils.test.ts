import {starGithubRepo, starMultipleRepos} from './starringUtils'

describe('starringUtil', () => {
    describe('starMultipleRepos', () => {
        it('should return the repo array with all starred items for the user', () => {
            const input = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            const expectation = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            expect(starMultipleRepos(input, [12, 34])).toStrictEqual(expectation)
        })
    })

    describe('starGithubRepo', () => {
        it('should return the repo array with starred items for the user', () => {
            const input = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            const expectation = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    starred: false,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            expect(starGithubRepo(input, 12)).toStrictEqual(expectation)
        })

        it('should return the same array if the id was not found', () => {
            const input = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            const expectation = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    starred: false,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    starred: false,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            expect(starGithubRepo(input, 123)).toStrictEqual(expectation)
        })

        it('should remove starring of repo that was previously starred', () => {
            const input = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            const expectation = [
                {
                    id: 12,
                    full_name: 'test_1',
                    html_url: 'url_1',
                    description: 'desc_01',
                    stargazers_count: 2,
                    starred: false,
                    owner: {
                        id: 1,
                        login: 'test_1',
                        avatar_url: 'avatar_1',
                        url: 'url_1'
                    }
                }, {
                    id: 34,
                    full_name: 'test_2',
                    html_url: 'url_2',
                    description: 'desc_02',
                    stargazers_count: 2,
                    starred: true,
                    owner: {
                        id: 1,
                        login: 'test_2',
                        avatar_url: 'avatar_2',
                        url: 'url_2'
                    }
                }]
            expect(starGithubRepo(input, 12)).toStrictEqual(expectation)
        })
    })
})