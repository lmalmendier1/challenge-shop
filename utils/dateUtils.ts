// TODO: add tests
export const pastDateByDays = ( days : number ) : Date => {
    return new Date(Date.now() - days * 24 * 60 * 60 * 1000)
}