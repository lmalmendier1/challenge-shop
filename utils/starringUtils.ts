import {GithubRepoItem} from '@interfaces/global'

export const starGithubRepo = (repositories: GithubRepoItem[], id: number): GithubRepoItem[] => repositories.map((repo) => ({
    ...repo,
    starred: repo.id === id ? !repo.starred : (repo.starred ?? false)
}))

export const starMultipleRepos = (repositories: GithubRepoItem[], ids: number[]): GithubRepoItem[] => ids.reduce((repos: GithubRepoItem[], id) => starGithubRepo(repos, id), repositories)