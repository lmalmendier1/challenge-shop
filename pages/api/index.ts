import {GithubRepoItem} from '@interfaces/global';
import {pastDateByDays} from '@utils/dateUtils'

// TODO: add option to pass in search params
export const fetchApiData = async (): Promise<GithubRepoItem[]> => {
    const lastWeekDate = pastDateByDays(7).toISOString().substring(0,10)
    const response = await fetch(`https://api.github.com/search/repositories?q=created:>${lastWeekDate}&sort=stars&order=desc`)

    const responseJson = response.status !== 204 ? await response.json() : []
    return responseJson.items
}