import styles from '@styles/Home.module.css'
import {fetchApiData} from './api'
import {GithubRepoItem, STORAGE_KEY} from '@interfaces/global'
import RepoItem from '@components/RepoItem'
import {useEffect, useState} from "react"
import {starMultipleRepos} from '@utils/starringUtils'

type HomeProps = {
    items: GithubRepoItem[]
}

export default function Home(props: HomeProps) {
    const [customerStars, starItems] = useState<number[]>([])
    const [starredOnly, showStarredOnly] = useState<boolean>(false)
    const {
        items
    } = props

    const updateCustomerStars = (id: number) => {
        const updatedStars : number[] = (customerStars || []).includes(id) ? customerStars.filter(i => i !== id) : [...customerStars, id]
        localStorage.setItem(STORAGE_KEY, JSON.stringify(updatedStars))
        starItems(updatedStars)
    }

    useEffect(() => {
        const customerStarredIds = JSON.parse(localStorage.getItem(STORAGE_KEY) ?? '[]')
        starItems(customerStarredIds)
    }, [])

    const starredItems = starMultipleRepos(items, customerStars)
    return (
        <div className={styles.container}>
            <main className={styles.main}>
                <h1 className={styles.title}>
                    Repos:
                </h1>
                <div>
                    <button onClick={() => showStarredOnly(!starredOnly)}>My Starred
                        Repos: {`${starredOnly ? '★' : '☆'}`}</button>
                </div>

                <ul className={styles.grid}>
                    {starredItems.map((repoItem: GithubRepoItem) => <RepoItem key={repoItem.id} {...repoItem} starredOnly={starredOnly}
                                                                              updateCustomerStars={() => updateCustomerStars(repoItem.id)}/>)}
                </ul>
            </main>

            <footer className={styles.footer}>
                <a
                    href="https://lucmalmendier.com/"
                    target="_blank"
                >
                    Luc Malmendier
                </a>
            </footer>
        </div>
    )
}


export async function getServerSideProps() {
    return {
        props: {
            items: await fetchApiData()
        },
    }
}
