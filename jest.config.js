const tsconfig = require('./tsconfig.json')
const paths = tsconfig.compilerOptions.paths

const moduleNameMapper = Object.keys(paths).reduce((acc, curr) => {
    const modulePaths = {
        ...acc,
        [curr]: './' + paths[curr]
    }
    return modulePaths
}, {})

module.exports = {
    preset: 'ts-jest',
    "roots": [
        "./"
    ],
    "testMatch": [
        "**/__tests__/**/*.+(ts|tsx|js)",
        "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.json'
        }
    },

    moduleNameMapper
}