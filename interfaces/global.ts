export const STORAGE_KEY = 'CUSTOMER_STARS'

export type GithubRespone = {
    items: GithubRepoItem[],
    status: number
}

export type GithubRepoItem = {
    id: number,
    full_name: string,
    owner: GithubOwner,
    html_url: string,
    description: string,
    stargazers_count: number,
    starred?: boolean
}

export type GithubOwner = {
    id: number,
    login: string,
    avatar_url: string,
    url: string,
}