import React from 'react'
import {GithubRepoItem} from '@interfaces/global'
import styles from '@styles/Home.module.css'

type RepoItemProps = GithubRepoItem & {
    starredOnly: boolean,
    updateCustomerStars: () => void
}

const RepoItem = (props: RepoItemProps) => {
    const {
        id,
        full_name,
        owner,
        html_url,
        description,
        stargazers_count,
        starred,
        updateCustomerStars,
        starredOnly
    } = props

    const {
        login,
        avatar_url,
        url
    } = owner
    const showItem = starredOnly ? starred : true;

    return (
        <li>
            {showItem &&
            <div className={styles.card}>
                <div>
                    <h2><a href={url}>{login}</a></h2>
                    <img src={avatar_url}/>
                </div>
                <br/>
                <div>
                    <h3><a href={html_url}>{`${login}: ${full_name}`}</a></h3>
                    <p>{description}</p>
                    <button onClick={updateCustomerStars}>{`${stargazers_count} ${starred ? '★' : '☆'}`}</button>
                </div>
            </div>
            }
        </li>
    )
}

export default RepoItem
